<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8" />
		<link rel="stylesheet" href="new 1.css" />
		<title>C'est tout moi!</title>
	</head>
	<body><h1>C'est tout moi!</h1>
		<h3>Identification</h3>
			Noms: NZIENGUI DIPOUMA</br> Prénoms: Yorick Théophane</br> Téléphones: 062941149 / 077201393</br> E-mail: yoricknziengui@gmail.com</br> Ville: Libreville</br> Quartier: Charbonnages (Lac bleu)</br>
		<h3>Portrait</h3>
			<p><img src="Yon.jpg" width height="250"/>
			</p>
		<h3>Education</h3>
			<h4><ul>Parcours scolaire</ul></h4>
				<li>De 2001 à 2007: Classes de CP1 au CM2</li>
				<li>De 2007 à 2016: Classes de 6ème en Tle</li>
				<li>De 2016 à 2019: 1ère en 3ème année universitaire</li>
			<h4><ul>Diplomes obtenus</ul></h4>
				<li>Certificat d'étude primaire (CEP) en 2007</li>
				<li>Brevet d'étude du premier cycle en 2012</li>
				<li>Baccalauréat en 2016</li>
		<h3>Expérience professionnelle</h3>
			<p>Manutentionnaire pour la Sociciété de Brasseries du Gabon durant deux semaines pendant les vacances de 2015 dont le role était de décharger les contenaires de boisson.</p>
		<h3>Biographie</h3>
			<video src="Yorick Dipouma.mp4"controls loop width height="200" poster "sintel.jpg" class="flottant"></video>
			<p>NZIENGUI DIPOUMA Yorick Théophane est un jeune étudiant en troisième année à l'Institut National des Sciences de Gestion. Né à Port-Gentil d'une famille de sept enfants dont il est le cinquième, il y a grandi et fait tout son parcours scolaire depuis les classes du primaires jusqu'en Terminale.</br> Après l'obtention de son baccalauréat, il se rendit à la capitale Libreville afin de poursuivre ses études jusqu'à présent. Parmi les sept enfants de cette famille, il est le seul qui s'en sort actuellement avec le baccalauréat.</br> De ce fait il veut se battre afin de montrer l'exemple aux autres de la famille, leur apprendre à ne pas baisser les bras et se battre pour subvenir aux besoins de celle-ci.</br>
				C'est quelqu'un de sociable, aimable, respectueux et poli. Il sait se donner à fond lorsqu'il faut et pour les causes qui lui tiennent à coeur, il n'aime pas la routine donc adore relever des défis.</p></br>
		<p><h3>Joignez moi sur:</h3></p>
			<ul><a href="https://www.facebook.com/yorick.dipouma"> Facebook <target="_blank"> </a></ul>
			<ul><a href="https://www.instagram.com/yorickdipouma/"> Instagram <target="_blank"> </a></ul>
			<ul><a href="https://www.linkedin.com/in/yorick-dipouma-97108b18b/"> Linked In <target="_blank> </a></ul>
		<h3>Centres d'intéret</h3>
			<li>Ecouter de la musique</li>
			<li>Passer du temps sur internet</li>
			<li>Regarder des séries</li>
			<li>Manger</li>
		<h3>Contenu multimédia</h3>
			<li>Musique préférée:
			<strong><em>Love Riddim de Rotimi</em></strong></li></br>
				<audio src="Rotimi.mp3"controls loop></audio></br>
			<li>Héro préféré: <strong>Batman<strong></li></br>
			<p><img src="BatMan.jpg"alt="Photo de Batman" width height="200"/></p>
		
	</body>
</html>